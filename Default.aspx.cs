﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using System.IO;

public partial class _Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        MembershipUser usr = Membership.GetUser();
        if (usr != null)
        {
            string sourceDir = string.Empty;
            string path = "Images/" + usr.ProviderUserKey.ToString();
            sourceDir = Server.MapPath(path);
            string[] Photos = Directory.GetFiles(sourceDir);
            foreach (string photo in Photos)
            {
                string photoName = photo.Substring(photo.Length - 40);
                ltrPhotos.Text += "<li><img src=\"Images/" + usr.ProviderUserKey.ToString() + "/" + photoName + "\" /></li>";
            }
        }
        else
        {
            ltrPhotos.Text = "<li><img src=\"Images/slide1.jpg\" /></li><li><img src=\"Images/slide2.jpg\" /></li><li><img src=\"Images/slide3.jpg\" /></li>";
        }
    }    
}