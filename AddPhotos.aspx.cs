﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using System.IO;

public partial class AddPhotos : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        MembershipUser usr = Membership.GetUser();
        hdfUserId.Value = usr.ProviderUserKey.ToString();
        string sourceDir = string.Empty;
        string path = "Images/" + usr.ProviderUserKey.ToString();
        sourceDir = Server.MapPath(path);
        string[] Photos = Directory.GetFiles(sourceDir);
        foreach (string photo in Photos)
        {
            string photoName = photo.Substring(photo.Length - 40);
            Image img = new Image();
            Button btn = new Button();
            Literal ltr = new Literal();
            Literal ltr1 = new Literal();
            ltr.Text = "<div>";
            ltr1.Text = "</div>";
            btn.CommandArgument = photo;
            img.ImageUrl = "Images/" + usr.ProviderUserKey.ToString() + "/" + photoName; ;
            btn.Click += new EventHandler(this.DeletePhoto);
            pnlImages.Controls.Add(ltr);
            pnlImages.Controls.Add(img);
            pnlImages.Controls.Add(btn);
            pnlImages.Controls.Add(ltr1);

        }
    }

    protected void DeletePhoto(object sender, EventArgs e)
    {
        Button btn = (Button)sender;
        File.Delete(btn.CommandArgument);
        Response.Redirect("AddPhotos.aspx");

    }
     
}