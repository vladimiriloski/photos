﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="LogIn.aspx.cs" Inherits="LogIn" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div class="main panel-register">
        <asp:Login ID="Login1" runat="server" Width="100%">
            <LayoutTemplate>
                <div class="form-layout">
                    <h1>Login</h1>
                    <div class="form-item">
                        <asp:RequiredFieldValidator ID="UserNameRequired" runat="server" ControlToValidate="UserName" ErrorMessage="User Name is required." ToolTip="User Name is required." ValidationGroup="Login1">*</asp:RequiredFieldValidator>
                        <asp:Label ID="UserNameLabel" runat="server" AssociatedControlID="UserName">User Name:</asp:Label>
                        <asp:TextBox ID="UserName" runat="server"></asp:TextBox>
                    </div>
                    <div class="form-item">
                        <asp:RequiredFieldValidator ID="PasswordRequired" runat="server" ControlToValidate="Password" ErrorMessage="Password is required."  ToolTip="Password is required." ValidationGroup="Login1">*</asp:RequiredFieldValidator>
                        <asp:Label ID="PasswordLabel" runat="server" AssociatedControlID="Password">Password:</asp:Label>
                        <asp:TextBox ID="Password" runat="server" TextMode="Password"></asp:TextBox>
                    </div>
                    <div class="form-item-rememberMe">
                        <asp:CheckBox ID="RememberMe" runat="server" Text="Remember me next time." />
                    </div>
                    <div>
                        <asp:Button ID="LoginButton" runat="server" CommandName="Login" Text="Log In" CssClass="button" ValidationGroup="Login1" />
                    </div>
                    <div class="loginPage-notifiaction">
                        <asp:Literal ID="FailureText" runat="server" EnableViewState="False"></asp:Literal>
                    </div>
                </div>            
            </LayoutTemplate>
        </asp:Login>
    </div>
</asp:Content>

