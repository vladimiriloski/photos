﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <link rel="stylesheet" href="FlexSlider/flexslider.css" type="text/css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js"></script>
<script src="FlexSlider/jquery.flexslider.js"></script>
<!-- Place in the <head>, after the three links -->
<script type="text/javascript" charset="utf-8">
    $(window).load(function () {
        $('.flexslider').flexslider({
           animation: "fade",              //String: Select your animation type, "fade" or "slide"
           slideDirection: "horizontal",   //String: Select the sliding direction, "horizontal" or "vertical"
           slideshow: true,                //Boolean: Animate slider automatically
           slideshowSpeed: 5000,           //Integer: Set the speed of the slideshow cycling, in milliseconds
           animationDuration: 600,         //Integer: Set the speed of animations, in milliseconds
           directionNav: true,             //Boolean: Create navigation for previous/next navigation? (true/false)
           controlNav: true,               //Boolean: Create navigation for paging control of each clide? Note: Leave true for manualControls usage
           keyboardNav: true,              //Boolean: Allow slider navigating via keyboard left/right keys
           mousewheel: false,              //Boolean: Allow slider navigating via mousewheel
           prevText: "Previous",          
           nextText: "Next",           
        });
    });
</script>
<div class="flexslider">
  <ul class="slides"> 
      <asp:Literal ID="ltrPhotos" runat="server"></asp:Literal>
  </ul>
</div>
</asp:Content>

