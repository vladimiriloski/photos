﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;

public partial class Register : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void CreateUserWizard1_CreatedUser(object sender, EventArgs e)
    {
        MembershipUser usr = Membership.GetUser(CreateUserWizard1.UserName);
        string folder = usr.ProviderUserKey.ToString(); 
        string savepath = "";
        string tempPath = "Images/" + folder;
        savepath = Server.MapPath(tempPath);
        System.IO.Directory.CreateDirectory(savepath);
    }
    protected void ContinueButton_Click(object sender, EventArgs e)
    {
        Response.Redirect("Default.aspx");
    }
}