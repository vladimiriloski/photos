﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;

public partial class MasterPage : System.Web.UI.MasterPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        MembershipUser user = Membership.GetUser();
        if (user != null)
        {
            Label lblUser = (Label)LoginView1.FindControl("lblUser");
            lblUser.Text = user.UserName;
            if (HttpContext.Current.Request.Url.AbsolutePath.ToLower().Contains("addphotos"))
            {
                HyperLink hpl = (HyperLink)LoginView1.FindControl("hplAddPhotos");
                hpl.Visible = false;
                HyperLink hpl1 = (HyperLink)LoginView1.FindControl("hplMyAlbum");
                hpl1.Visible = true;
            }
        }
    }
}
