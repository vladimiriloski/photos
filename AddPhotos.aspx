﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="AddPhotos.aspx.cs" Inherits="AddPhotos" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <link rel="Stylesheet" type="text/css" href="snippets/css/img-upload.css" />
    <script type="text/javascript" src="uploadify/jquery-1.4.2.min.js"></script>
    <script type="text/javascript" src="uploadify/swfobject.js"></script>
    <script type="text/javascript" src="uploadify/jquery.uploadify.v2.1.4.min.js"></script>   
    <script type="text/javascript">
    // <![CDATA[

            $(document).ready(function () {
                $('#FileUpload').uploadify(
                    {
                        'uploader': 'uploadify/uploadify.swf',
                        'script': 'Upload.ashx?uid=' + $('#hdfUserId').val(),
                        'cancelImg': 'uploadify/cancel.png',
                        'auto': true,
                        'multi': true,
                        'fileExt': '*.jpg;*.gif;*.png',
                        'fileDesc': 'Image Files(jpg,gif,png)',
                        'sizeLimit': 1048576,
                        'queueSizeLimit': 10,
                        'buttonText': "Add Photos",
                        'onAllComplete': function (event, data) {
                            $(location).attr('href', 'AddPhotos.aspx');
                        }
                    });
                $("#startUploadLink").click(function () {
                    $('#FileUpload').uploadifyUpload();
                    return false;
                });
            });
    // ]]>
     </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div class="main panel-addImage uploadImg-wrapper">
        <h1>Add New Photos</h1>
        <asp:FileUpload ID="FileUpload" ClientIDMode="Static" runat="server" />
        <asp:HiddenField ID="hdfUserId" ClientIDMode="Static" runat="server" />
        <asp:Panel ID="pnlImages" runat="server">
        </asp:Panel>
    </div>
</asp:Content>

