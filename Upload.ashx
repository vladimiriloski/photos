﻿<%@ WebHandler Language="C#" Class="Upload" %>

using System;
using System.Web;
using System.IO;
using System.Web.Security;
using System.Web.SessionState;

public class Upload : IHttpHandler, IReadOnlySessionState  
{
    public void ProcessRequest(HttpContext context)
    {          
        context.Response.ContentType = "text/plain";
        context.Response.Expires = -1;
        try
        {
            string folder = context.Request.QueryString["uid"];        
            HttpPostedFile postedFile = context.Request.Files["Filedata"];
            string filename = postedFile.FileName;
            string savepath = "";
            string tempPath = "Images/" + folder;
            savepath = context.Server.MapPath(tempPath);
            Directory.CreateDirectory(savepath + "/");
            string extension = Path.GetExtension(filename);
            System.Guid image_id = System.Guid.NewGuid();
            postedFile.SaveAs(savepath + "/" + image_id + extension);
            context.Response.StatusCode = 200;
            context.Response.Write("Images/" + image_id + extension);
        }
        catch (Exception ex)
        {
            context.Response.Write(ex);
        }

    }
 
    public bool IsReusable {
        get {
            return false;
        }
    }

}